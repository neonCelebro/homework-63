import React, { Component, Fragment } from 'react';
import {Switch, Route} from 'react-router-dom';
import Tasks from './containers/TaskContainer.js';
import Films from './containers/FilmContainer.js';
import AppControl from './components/AppControl/AppControl';


class App extends Component {
  render() {
    return (
      <Fragment>
        <AppControl />
        <Switch>
          <Route path='/tasks' component={Tasks} />
          <Route path='/films' component={Films} />
        </Switch>
      </Fragment>
    );
  }
};



export default App;
