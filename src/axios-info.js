import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://tasks-and-films.firebaseio.com/' // Your URL here!
});

export default instance;
