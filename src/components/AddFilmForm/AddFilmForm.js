import React, { Component } from 'react';
import './AddFilmForm.css';


class AddFilmForm extends Component {
  render(){
    return (
      <div>
        <form className='boxForm' onSubmit={this.props.submit}>
          <span className='cost'>
            <input
              value={this.props.value}
              onChange={this.props.change}
              maxLength='50'
              required id='itemName'
              placeholder='Фильм который я хочу посмотреть'
              type='text'
            />
          </span>
          <input type='submit' id='sendNewData' value ='Отправить'/>
        </form>
      </div>
    )
  }
};

export default AddFilmForm;
