import React from 'react';
import './AddTaskForm.css';

const AddTaskForm = (props) =>

  <form className="boxForm">
    <input className="textNewTask" type="text" value={props.currentTask} onChange={props.changeTask} onFocus={props.focusTask}/>
    <button onClick={props.addTask} id="sendNewData" type="button" name="button">Send</button>
  </form>

export default AddTaskForm;
