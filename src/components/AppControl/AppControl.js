import React, {Component} from 'react';
import { NavLink } from 'react-router-dom';
import './AppControl.css';

class AppControl extends Component {
  render(){
    return(
      <div>
        <NavLink className='btn' to="/tasks">tasks</NavLink>
        <NavLink  className='btn' to="/films">films</NavLink>
      </div>
    )
  }
};

export default AppControl;
