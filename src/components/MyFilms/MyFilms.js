import React, { PureComponent } from 'react';
import './MyFilms.css';
import Task from '../Task0/Task';


class MyFilms extends PureComponent {
  render(){
  if (this.props.films.length === 0) {
  return <div className="container">У Вас пока нет записей</div>
  }
  return (
    <div className="container">

      {this.props.films.map((film, index) =>{
        return <Task
          key={index}
          change={(event)=> this.props.change(event, index)}
          film={film}
          cliked={()=> this.props.cliked(index)}
               />
      })}
    </div>
  )
}
};

export default MyFilms;
