import React, { PureComponent } from 'react';
import Task from '../Task/Task';

class MyTasks extends PureComponent {
  render(){
  return (
    <div>
      {this.props.tasks.map((task, index) => <Task redy={task.status} remove={()=>this.props.remove(index)}
        status={()=> this.props.status(index)}
        textTask={task.textTask}
        key={index} />)}
    </div>
    );
  };
};

export default MyTasks;
