import React, { Component } from 'react';
import AddFilmForm from '../components/AddFilmForm/AddFilmForm';
import MyFilms from '../components/MyFilms/MyFilms';
import axios from '../axios-info';
import Spinner from '../components/UI/Spinner/Spinner';

class Films extends Component {
  state = {
    films: [],
    filmToWath: '',
    loading: false,
  }

  setInfoInState = () =>{
    this.setState({loading : true});
    axios.get('/films.json')
    .then((response)=> {
      if (response.data){
        this.setState({films: response.data});
      }
    })
    .finally(this.setState({loading : false}));
  };

  newFilm = (e) => this.setState({filmToWath: e.target.value});

  changeFilm = (event, index) => {
    let films = [...this.state.films];
    let film = event.target.value;
    films[index] = film;
    this.setState({films})
  };

  addNewFilm = (e) => {
    this.setState({loading : true});
    e.preventDefault();
    const films = [...this.state.films];
    let filmToWath = this.state.filmToWath;
    films.push(filmToWath);
    axios.put('/films.json', films)
    .then(this.setState({films}))
    .finally(() =>{
      this.setState({films, filmToWath: '', loading : false});
    })
};

  remuveFilm = (index) =>{
    this.setState({loading : true})
    let films = [...this.state.films];
    films.splice(index, 1);
    axios.delete('/films/'+ index +'.json')
    .then(this.setState({films}))
    .then(axios.put('/films.json', films))
    .finally(this.setState({films, loading:false}));
  };
  componentDidMount(){
    this.setInfoInState();
  };

  render() {
    if (this.state.loading) {
    return <Spinner />;
  }
    return (
      <div className="App">
        <AddFilmForm
          value={this.state.filmToWath}
          change={this.newFilm}
          submit={this.addNewFilm}
        />
        <MyFilms
          onServer={this.getInfoOnServer}
          films={this.state.films}
          change={this.changeFilm}
          cliked={this.remuveFilm}/>
      </div>
    );
  }
}

export default Films;
