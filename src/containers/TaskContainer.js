import React, { Component } from 'react';
import AddTaskForm from '../components/AddTaskForm/AddTaskForm.js';
import MyTasks from '../components/MyTasks/MyTasks';
import axios from '../axios-info';
import Spinner from '../components/UI/Spinner/Spinner';

class Tasks extends Component {
  state = {
    tasks : [],
    currentTask: 'New Task',
    loading: false,
  };

  changeTaskText = (event) => this.setState({currentTask: event.target.value});

  clearTaskText = () => this.setState({currentTask: ''});

  returnText = () => this.setState({currentTask: 'New Task'});

  removeTaskHandler = (id) => {
    this.setState({loading : true});
    const index = this.state.tasks.findIndex(p => p.id === id);
    let tasks = [...this.state.tasks];
    tasks.splice(index, 1);
      axios.delete('/tasks/'+id +'.json')
      .then(this.setState({tasks}))
      .then(axios.put('/tasks.json', tasks))
      .finally(this.setState({loading:false}));
    };
    redyTask = (index) => {
      this.setState({loading : true});
      const tasks = [...this.state.tasks];
      tasks[index].status = !tasks[index].status;
      axios.put('/tasks/' + index + '/status' + '.json', tasks[index].status)
      .then(this.setState({tasks}))
      .finally(this.setState({loading:false}));
    };
    addNewTaskHandler = () =>{
      this.setState({loading : true});
      const tasks = [...this.state.tasks];
      const newTask = {
        textTask: this.state.currentTask,
        id: Date.now(),
        status: false,
        };
      tasks.push(newTask);
      axios.put('/tasks.json', tasks)
      .then(this.setState({tasks}))
      .finally(() =>{
        this.returnText();
        this.setState({loading : false});
      })
    };
    setInfoInState = () =>{
      this.setState({loading: true});
        axios.get('/tasks.json')
        .then((response)=> {
          if (response.data){
            this.setState({tasks: response.data});
          }
        })
        .finally(this.setState({loading : false}));
    };
    componentDidMount(){
      this.setInfoInState();
    };

    render() {
      if (this.state.loading) {
      return <Spinner />;
    }

    return (
      <div>
        <AddTaskForm
          currentTask={this.state.currentTask}
          changeTask={this.changeTaskText}
          focusTask={this.clearTaskText}
          addTask={this.addNewTaskHandler}/>
        <MyTasks
          onServer={this.getInfoOnServer}
          tasks={this.state.tasks}
          remove={this.removeTaskHandler}
          status={this.redyTask}
        />
      </div>
    );
  }
}

export default Tasks;
